package interfaces;

public interface GoodsInterface
{
    int getId();
    String getBrand();
    int getPrice();
    String getSpecification();
    String getTypeOfGood();
}
