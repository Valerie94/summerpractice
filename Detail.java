package controller;

import interfaces.DetailInterface;

public class Detail implements DetailInterface {

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
