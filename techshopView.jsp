<%--
    Document   : detailsView
    Created on : 17.07.2013, 0:45:45
    Author     : Artyom
--%>

<%@page import="org.springframework.core.io.FileSystemResource"%>
<%@page import="org.springframework.beans.factory.BeanFactory"%>
<%@page import="org.springframework.context.support.ClassPathXmlApplicationContext"%>
<%@page import="model.*"%>
<%@page import="interfaces.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>InternetShop</title>
    </head>
    <body>
        <h1>List of Goods</h1>
        <spring:nestedPath path="detail">
            <form action="" method="post">
                <spring:bind path="value">
                    <h2>Processors</h2>
                    <table border="1">
                        <tr><th>ID<th>TypeOfGood<th>Brand<th>Specification<th>Price
                        <%
                            ProcessorsInterface pc = (ProcessorsInterface) (new ClassPathXmlApplicationContext("/beans/beans.xml").getBean("Processors"));
                           for (int i = 1;i<=5;i++)
                           {
                               out.println("<tr><td><input type='submit' name ='value' value='" + i + "'>" + pc.printList(i));
                           }
                        %>
                    </table>
                    <h2>VideoCards</h2>
                      <table border="1">
                        <tr><th>ID<th>TypeOfGood<th>Brand<th>Specification<th>Price
                        <%
                        VideoCardsInterface vc = (VideoCardsInterface) (new ClassPathXmlApplicationContext("/beans/beans.xml").getBean("VideoCards"));
                           for (int i = 1;i<=5;i++)
                           {
                               out.println("<tr><td><input type='submit' name ='value' value='" + i + "'>" + vc.printList(i));
                           }
                        %>
                    </table>
                </spring:bind>
            </form>
        </spring:nestedPath>
    </body>
</html>
