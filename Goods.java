package data;

import interfaces.GoodsInterface;

public class Goods implements GoodsInterface
{
    public int id;
    public String typeOfGood;
    public int price;
    public String brand;
    public String specification;

    public Goods(int id, String typeOfGood,int price,String brand,String specification)
    {
        this.id = id;
        this.typeOfGood = typeOfGood;
        this.price = price;
        this.brand = brand;
        this.specification = specification;
    }

    public int getId() {
        return id;
    }

    public String getBrand() {
        return brand;
    }

    public int getPrice() {
        return price;
    }

    public String getSpecification() {
        return specification;
    }

    public String getTypeOfGood() {
        return typeOfGood;
    }
}

