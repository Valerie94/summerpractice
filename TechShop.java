package TechShop;

import java.util.*;

public class TechShop 
{   
    public static void searching(Device dev, String type, String brand, int price )
    {
        Goods good;
        List<Goods> list = dev.getByAll(type, brand, price);
        for (int i=0;i<list.size();i++) 
        {
            good = list.get(i);
            System.out.println(good.typeOfGood+ "," + good.brand+ "," + good.price+ "," + good.specification);
        }
        if (list.isEmpty()) {
            System.out.println("Not Found");
        }
    }
    
    public static void printData(Device dev)
    {
        Goods good;
        List<Goods> list = dev.GetAll();

        for (int i=0;i<list.size();i++) 
        {
            good = list.get(i);
            System.out.println(good.typeOfGood+ "," + good.brand+ "," + good.price+ "," + good.specification);
        }
    }
    
    
    public static void main(String[] args) 
    {
        Goods good;
        VideoCards vc = new VideoCards();
        System.out.println("Get All VideoCards");
        printData(vc);
        vc.AddNewOneGood(5, "Videocard", 9490, "Geforce", "1024 mb");
        System.out.println("");
        
        System.out.println("Get All VideoCards after Update");
        printData(vc);
        Processors ps = new Processors();
        System.out.println("");
        
        System.out.println("Get All Processors");
        printData(ps);
        ps.AddNewOneGood(5, "Processor", 7290, "Celeron", "2.4 Ghz x 4 core");
        System.out.println();
        
        System.out.println("Get All Processors After Update");
        printData(ps);
        System.out.println();
        
        System.out.println("Find Processor Xeon, Price = 8290");
        searching(ps,"Processor","Xeon", 8290);
        System.out.println();
        
        System.out.println("Find VideoCard Gainward, Price = 11480");
        searching(vc,"VideoCard", "Gainward", 11480);   
        System.out.println();  
    }
}
