package controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import service.DetailService;
import interfaces.TechshopInterface;

public class TechshopController extends SimpleFormController implements TechshopInterface{

    private DetailService detailService;

    public void setTechshopService(DetailService techshopService)
    {
        this.detailService = detailService;
    }


    public TechshopController() {
        //Initialize controller properties here or
        //in the Web Application Context
        setCommandClass(Detail.class);
        setCommandName("detail");
        setSuccessView("detailsView");
        setFormView("techshopView");
    }

//    @Override
//    protected void doSubmitAction(Object command) throws Exception {
//        throw new UnsupportedOperationException("Not yet implemented");
//    }
    //Use onSubmit instead of doSubmitAction
    //when you need access to the Request, Response, or BindException objects

     @Override
     public ModelAndView onSubmit(
     HttpServletRequest request,
     HttpServletResponse response,
     Object command,
     BindException errors) throws Exception {
     ModelAndView mv = new ModelAndView(getSuccessView());
     Detail detail = (Detail) command;
     mv.addObject("details", DetailService.getDetailsProc(Integer.parseInt(detail.getValue())));
     mv.addObject("details", DetailService.getDetailsVidCards(Integer.parseInt(detail.getValue())));
     return mv;
     }

}
