package model;

import data.*;
import java.util.*;
import interfaces.VideoCardsInterface;

public class VideoCards extends Device implements VideoCardsInterface
{
    public VideoCards()
    {
        this.AddGoods();
    }

    public void AddGoods()
    {
       dataList = new ArrayList<Goods>();
       this.dataList.add(new Goods(id,"", 11480, "", ""));
       this.dataList.add(new Goods(id++,"VideoCard", 4490, "Sapphire", "HD7770 GHZ <HD7770. GDDR5. 128 bit. DVI. HDMI. 2*mini DP. Lite Retail>"));
       this.dataList.add(new Goods(id++,"VideoCard", 3370, "PowerColor", "AX7750 1GBD5-DH <HD7750. GDDR5. 128 bit. HDCP. DVI. 4*mini DP. OEM>"));
       this.dataList.add(new Goods(id++,"VideoCard", 1600, "Zotac", "GT610 Synergy Edition c CUDA <GFGT610. GDDR3. 64 bit. HDCP. VGA. DVI. HDMI. Low Profile. Retail>"));
       this.dataList.add(new Goods(id++,"VideoCard", 11480, "Gainward", "GTX570 Phantom c CUDA <GFGTX570. GDDR5. 320 bit. 2*DVI. HDMI. DP. Retail>"));
       this.dataList.add(new Goods(id++,"VideoCard",5600,"Geforce","GT9800 Sapphire c CUDA <GFGT550. GDDR5. 320bit. HDMI. 2*DVI. DP>"));
    }

        @Override
    public String printList(int id)
    {
        String result = "";
        if (id<1 | id>=dataList.size())
            result = null;
        else result += "<td>" + dataList.get(id).getTypeOfGood()+ "<td>" + dataList.get(id).getBrand()+"<td>"+dataList.get(id).getSpecification() + "<td>" + dataList.get(id).getPrice();
        return result;
    }

    @Override
    public String[] printList()
    {
        String[] result = new String[5];
        for (int i=0;i<dataList.size();i++)
            result[i]=printList(i);
        return result;
    }


}
