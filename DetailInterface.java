
package interfaces;

public interface DetailInterface
{
    String getValue();
    void setValue(String value);
}
