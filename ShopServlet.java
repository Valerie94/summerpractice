package TechShop;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

public class ShopServlet extends HttpServlet {
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here*/
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ShopServlet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ShopServlet at " + request.getContextPath () + "</h1>");
			VideoCards vc = new VideoCards();
			TechShop ts = new TechShop();
			Goods good;
			out.println("<h1>List Of VideoCards:</h1>");
			List<Goods> list = vc.GetAll();
			for (int i=0;i<list.size();i++) 
			{
				good = list.get(i);
				out.println("<p>" + good.typeOfGood+ "," + good.brand+ "," + good.price+ "," + good.specification + "</p>");
			}
			if (list.isEmpty()) {
				out.println("Not Found");
			}
			out.println("<h1>List Of Processors:</h1>");
			Processors ps = new Processors();
			list = ps.GetAll();
						for (int i=0;i<list.size();i++) 
			{
				good = list.get(i);
				out.println("<p>" + good.typeOfGood+ "," + good.brand+ "," + good.price+ "," + good.specification + "</p>");
			}
			if (list.isEmpty()) {
				out.println("Not Found");
			}
            out.println("</body>");
            out.println("</html>");
            
        } 
		finally 
		{ 
            out.close();
        }
    } 

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException 
	{
        processRequest(request, response);
    } 

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException 
	{
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() 
	{
        return "Short description";
    }// </editor-fold>

}
