package model;

import data.Goods;
import java.util.*;
import interfaces.DeviceInterface;

public abstract class Device implements DeviceInterface
{
    List<Goods> dataList = new ArrayList<Goods>();

    protected static int id = 0;

    abstract public void AddGoods();

    public void AddNewOneGood(int id, String type, int price, String brand, String specification)
    {
        dataList.add(new Goods(id++, type, price, brand, specification));
    }

    public String printList(int id)
    {
        String result = "";
        if (id<1 | id>=dataList.size())
            result = null;
        else result += "<td>" + dataList.get(id).getTypeOfGood()+ "<td>" + dataList.get(id).getBrand()+"<td>"+dataList.get(id).getSpecification() + "<td>" + dataList.get(id).getPrice();
        return result;
    }

    public String[] printList()
    {
        String[] result = new String[5];
        for (int i=0;i<dataList.size();i++)
            result[i]=printList(i);
        return result;
    }



    public List<Goods> GetAll()
    {
        return dataList;
    }


    public List<Goods> getByAll(String type, String brand, int price)
    {
        List<Goods> resultOfSearch;
        resultOfSearch = getByType(type);
        if (resultOfSearch==null)
            return null;
        else
        {
            resultOfSearch = getByBrand(resultOfSearch,brand);
            if (resultOfSearch==null)
                {
                    return null;
                }
            else
                {
                   resultOfSearch = getByPrice(resultOfSearch,price);
                }
            if (resultOfSearch==null)
                return null;
        }
        return resultOfSearch;
    }

    public List<Goods> getByType(String type)
    {
        List<Goods> result = new ArrayList<Goods>();
        Goods item;
        String title;
        for (int i=0 ; i<dataList.size() ; i++)
        {
           item = dataList.get(i);
           title = item.typeOfGood;
        if (title.equals(type))
            result.add(dataList.get(i));
        }
        return result;
    }


    public List<Goods> getByBrand(List<Goods> list, String brand)
    {
        List<Goods> result = new ArrayList<Goods>();
        Goods item;
        String Brand;
        for (int i=0 ; i<list.size() ; i++)
        {
           item = list.get(i);
           Brand = item.brand;
        if (Brand.equals(brand)) {
                result.add(list.get(i));
            }
        }
        return result;
    }

     public List<Goods> getByPrice(List<Goods> list, int price)
        {
            List<Goods> result = new ArrayList<Goods>();
            Goods item;
            int Price;
            for (int i=0 ; i<list.size() ; i++)
            {
               item = list.get(i);
               Price = item.price;
            if (Price==price) {
                    result.add(list.get(i));
                }
            }
            return result;
        }
}

