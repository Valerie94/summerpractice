package interfaces;

import service.DetailService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

public interface TechshopInterface
{
    void setTechshopService(DetailService techshopService);

     ModelAndView onSubmit(
     HttpServletRequest request,
     HttpServletResponse response,
     Object command,
     BindException errors) throws Exception;

}
