package service;

import interfaces.ProcessorsInterface;
import interfaces.VideoCardsInterface;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class DetailService {

    public static String getDetailsProc(int id)
    {
        ProcessorsInterface pc = (ProcessorsInterface) (new ClassPathXmlApplicationContext("/beans/beans.xml").getBean("Processors"));
        return pc.printList(id);
    }

    public static String getDetailsVidCards(int id)
    {
        VideoCardsInterface vc = (VideoCardsInterface) (new ClassPathXmlApplicationContext("/beans/beans.xml").getBean("VideoCards"));
        return vc.printList(id);
    }
}
