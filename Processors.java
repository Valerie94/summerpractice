package model;

import java.util.*;
import data.Goods;
import interfaces.ProcessorsInterface;

public class Processors extends Device implements ProcessorsInterface
{
    public Processors()
    {
        this.AddGoods();
    }

    public void AddGoods()
    {
       dataList = new ArrayList<Goods>();
       this.dataList.add(new Goods(id,"",0,"",""));
       this.dataList.add(new Goods(id++,"Processor", 2569, "Pentium", "G630 BOX <2.70GHz. 3Mb. LGA1155 (Sandy Bridge)>"));
       this.dataList.add(new Goods(id++,"Processor", 11190, "Core i7", "Core i7-2600S OEM <2.80GHz. 8Mb. 65W. LGA1155 (Sandy Bridge)>"));
       this.dataList.add(new Goods(id++,"Processor", 9390, "Core i5", "2500K BOX <3.30GHz. 6Mb. LGA1155 (Sandy Bridge)>"));
       this.dataList.add(new Goods(id++,"Processor", 8290, "Xeon", "1220 OEM <3.10GHz. 8M Cache. Socket1155>"));
       this.dataList.add(new Goods(id++,"Processor", 6000, "Intel", "Intel Core 2 Duo Extreme <2x3.70GHz. 8M Cache. Socket 1155>"));
    }

    @Override
    public String printList(int id)
    {
        String result = "";
        if (id<1 | id>=dataList.size())
            result = null;
        else result += "<td>" + dataList.get(id).getTypeOfGood()+ "<td>" + dataList.get(id).getBrand()+"<td>"+dataList.get(id).getSpecification() + "<td>" + dataList.get(id).getPrice();
        return result;
    }

    @Override
    public String[] printList()
    {
        String[] result = new String[5];
        for (int i=0;i<dataList.size();i++)
            result[i]=printList(i);
        return result;
    }

}
