package interfaces;

import java.util.*;
import data.Goods;


public interface DeviceInterface
{
    void AddNewOneGood(int id, String type, int price, String brand, String specification);
    String printList(int id);
    String[] printList();
    List<Goods> GetAll();
    List<Goods> getByAll(String type, String brand, int price);
    List<Goods> getByType(String type);
    List<Goods> getByBrand(List<Goods> list, String brand);
    List<Goods> getByPrice(List<Goods> list, int price);
}
